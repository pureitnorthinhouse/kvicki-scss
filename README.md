### Kvicklight SASS Files

This is the repository for the scss fileset for the Kvicklight frontend and backend theme generator(scss-compiler).

This Directory contains 2 folders:

- `bootstrap` - Contains code for Bootstrap. The File bootstrap-grid.scss in included in our project under the `thtme/_external-libraries.scss` file.
- `theme` - Contains our actual scss code. To change any configurations like background-color, text-light, font-faces and all, create a variable for that in \_variables.scss and then use that in your code. if your using the `BE` branch, create that newly created variable as a key in your variable object that you write to this file.
